def test_expected_structure(host):
    assert host.file('/var/www/virtual/ansible/shared/.env').exists


def test_expected_content(host):
    assert host.file('/var/www/virtual/ansible/shared/.env')\
        .contains('APP_ENV=production')
