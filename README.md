# macrominds/provision/uberspace/web-provide-shared-deployer-dot-env

Prepare an U7 Uberspace server to provide a shared .env as used by our autodeployment: 

* copy a local `.env` template to the shared location (`shared/.env`)

## Requirements

Setup an [U7 Uberspace](https://dashboard.uberspace.de/register) and provide an ssh access to it.

## Role Variables

See [defaults/main.yml](defaults/main.yml). 

* `web_shared_app_env`: gets inserted into .env template APP_ENV={{ web_shared_app_env }}
  Defaults to `production`
* `web_shared_dot_env_local_template_path`: specify the local .env template location 
  Defaults to `templates/.env`
* `web_shared_html_base_path`: 
  Defaults to `/var/www/virtual/{{ ansible_facts.user_id }}`
* `web_shared_dot_env_remote_base_path`: 
  Defaults to `{{ web_shared_html_base_path }}/shared`
* `web_shared_dot_env_remote_path`: 
  Defaults to `{{ web_shared_dot_env_remote_base_path }}/.env`

## Example Playbook

### Prerequisites

In your project, provide the following files:

ansible.cfg

```ini
[defaults]
roles_path = $PWD/galaxy_roles:$PWD/roles
```

requirements.yml
 
```yaml
- src: git+https://gitlab.com/macrominds/provision/uberspace/web-provide-shared-deployer-dot-env.git
  path: roles
  name: web-provide-shared-deployer-dot-env
```

And run `ansible-galaxy install -r requirements.yml` to install 
or `ansible-galaxy install -r requirements.yml  --force` to upgrade.

### Playbook

```yaml
- hosts: all
  roles:
    - role: web-provide-shared-deployer-dot-env
```

## Testing

Test this role with `molecule test --all`.

## License

ISC

## Author Information

This role was created in 2019 by Thomas Praxl.
